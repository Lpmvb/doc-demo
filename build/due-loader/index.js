const { join } = require("path");
const { readFileSync, lstatSync, readdirSync } = require("fs");
const loaderUtils = require("loader-utils");
const matter = require("gray-matter");
const markdownIt = require("markdown-it");
const markdownContainer = require("markdown-it-container");
const mdUtils = require("markdown-it/lib/common/utils");
const hljs = require("highlight.js");
const mustache = require("mustache");
const { compileTemplate } = require("@vue/component-compiler-utils");
const compiler = require("vue-template-compiler");

const { unescapeAll, escapeHtml } = mdUtils;

module.exports = function(content) {
  if (this.cacheable) this.cacheable();

  const frontmatter = matter(content);
  const context = this.context;
  let importCode = "";
  let sourceCode = {};
  let importCount = 0;
  const md = markdownIt({
    html: true,
    highlight: function(str, lang) {
      if (lang && hljs.getLanguage(lang)) {
        try {
          return hljs.highlight(str, { language: lang }).value;
        } catch (__) {}
      }
      return ""; // use external default escaping
    },
  });
  md.renderer.rules.fence = customFence;
  md.use(markdownContainer, "demo", {
    validate: function(params) {
      return params.trim().match(/^demo\s*(.*)$/);
    },
    render: function(tokens, idx, options) {
      const m = tokens[idx].info.trim().match(/^demo\s*(.*)$/);

      if (tokens[idx].nesting === 1) {
        // TODO: alias support 暂只支持相对路径
        const componentPath = m[1];
        const path = join(context, componentPath);
        let indexPath = path;
        let filesPath = [indexPath];
        const isDirectory = lstatSync(path).isDirectory();
        if (isDirectory) {
          indexPath = join(path, "index.vue");
          filesPath = [indexPath];
          const files = readdirSync(path);
          filesPath = filesPath.concat(
            files
              .filter((item) => item !== "index.vue")
              .map((item) => join(path, item)),
          );
        }
        const code = readFileSync(indexPath, "utf-8");
        importCount += 1;
        importCode += `const demo${importCount} = require('${componentPath}').default;\nthis.dynamicComponents.demo${importCount} = demo${importCount};\n`;

        const source = [];
        filesPath.forEach((item) => {
          const fileContent = readFileSync(item, "utf-8");
          const pathSplit = item.split("/");
          const name = pathSplit[pathSplit.length - 1];
          const highlighted = options.highlight
            ? options.highlight(fileContent, "html") || escapeHtml(fileContent)
            : escapeHtml(fileContent);

          source.push({
            name: name,
            code: fileContent,
            highlighted: `<pre><code class="language-html">${highlighted}</code></pre>`,
          });
        });

        sourceCode[`demo${importCount}`] = source;

        return (
          `<code-demo :source="sourceCode.demo${importCount}">` +
          `<component :is="dynamicComponents.demo${importCount}" />`
        );
      }
      return "</code-demo>\n";
    },
  });

  const html = md.render(frontmatter.content);

  const compileOptions = {
    source: `<div class="markdown-body">${html}</div>`,
    filename: this.resourcePath,
    compiler,
    compilerOptions: {
      outputSourceRange: true,
    },
    transformAssetUrls: true,
    isProduction: process.env.NODE_ENV === "production",
  };

  const compiled = compileTemplate(compileOptions);

  const runtimeTpl = readFileSync(join(__dirname, "runtime.tpl"), "utf-8");

  const result = mustache.render(runtimeTpl, {
    importCode,
    code: compiled.code,
    data: JSON.stringify(frontmatter.data),
    sourceCode: JSON.stringify(sourceCode),
  });

  return result;
};

function customFence(tokens, idx, options, env, slf) {
  let token = tokens[idx],
    info = token.info ? unescapeAll(token.info).trim() : "",
    langName = "",
    langAttrs = "",
    highlighted,
    i,
    arr,
    tmpAttrs,
    tmpToken;

  if (info) {
    arr = info.split(/(\s+)/g);
    langName = arr[0];
    langAttrs = arr.slice(2).join("");
  }

  if (options.highlight) {
    highlighted =
      options.highlight(token.content, langName, langAttrs) ||
      escapeHtml(token.content);
  } else {
    highlighted = escapeHtml(token.content);
  }

  if (highlighted.indexOf("<pre") === 0) {
    return highlighted + "\n";
  }

  // If language exists, inject class gently, without modifying original token.
  // May be, one day we will add .deepClone() for token and simplify this part, but
  // now we prefer to keep things local.
  if (info) {
    i = token.attrIndex("class");
    tmpAttrs = token.attrs ? token.attrs.slice() : [];

    if (i < 0) {
      tmpAttrs.push(["class", options.langPrefix + langName]);
    } else {
      tmpAttrs[i] = tmpAttrs[i].slice();
      tmpAttrs[i][1] += " " + options.langPrefix + langName;
    }

    // Fake token just to render attributes
    tmpToken = {
      attrs: tmpAttrs,
    };

    return (
      "<pre><code v-pre" +
      slf.renderAttrs(tmpToken) +
      ">" +
      highlighted +
      "</code></pre>\n"
    );
  }

  return (
    "<pre><code v-pre" +
    slf.renderAttrs(token) +
    ">" +
    highlighted +
    "</code></pre>\n"
  );
}
