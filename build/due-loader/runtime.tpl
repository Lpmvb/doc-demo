function extractVueFunctions () {
  {{{ code }}}
  return {
    render: render,
    staticRenderFns: staticRenderFns
  }
}

const vueFunctions = extractVueFunctions()

module.exports = {
  component: {
    data: function () {
      return {
        templateRender: null,
        dynamicComponents: {},
        sourceCode: {{{ sourceCode }}},
      }
    },
    render: function (createElement) {
      return this.templateRender ? this.templateRender() : createElement("div", "Rendering");
    },
    created: function () {
      {{{ importCode }}}
      this.templateRender = vueFunctions.render;
      this.$options.staticRenderFns = vueFunctions.staticRenderFns;
    }
  },
  data: {{{ data }}}
}