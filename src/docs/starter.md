---
title: "起步"
order: 1
---

# 起步

## 简单的 Demo

* 支持文件及文件夹（需包含 index.vue）

```
::: demo ../components/Popup/message.vue（组件的相对路径）
:::
```

::: demo ../components/Popup/message.vue
:::

## 用了啥

* markdown-it 处理 Markdown 的
* gray-matter 处理 frontmatter  的
* highlight.js 处理代码高亮的
* mustache 模板生成代码的
* vue-template-compiler 编译 Vue template 的
* vue-cli
* windicss 写样式的（有点问题，样式冲突了）

## 参考了啥

* [Vuepress](https://vuepress.vuejs.org/)
* [Element UI](https://github.com/ElemeFE/element/tree/dev/build/md-loader)
* [gridsome / vue-remark-plugin](https://github.com/gridsome/gridsome/tree/master/packages/vue-remark)
* [mdx vue support](https://github.com/mdx-js/mdx/tree/master/examples/vue)
* [dumi](https://github.com/umijs/dumi)
* [naive-ui](https://github.com/TuSimple/naive-ui)