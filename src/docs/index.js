const modulesFiles = require.context('./', true, /\.md$/);

const components = {};
const docs = [];

modulesFiles.keys().reduce((modules, modulePath) => {
  const moduleName = modulePath.replace(/^\.\/(.*)\.\w+$/, '$1');
  const value = modulesFiles(modulePath);
  modules[moduleName] = value;
  components[moduleName] = value.component;
  docs.push({ name: moduleName, data: value.data });
  return modules;
}, {});

docs.sort((a, b) => a.data.order - b.data.order);

export { components, docs };

// group:
//   title: '分组'
//   order: 1
// nav:
//   title: '导航'
//   order: 1