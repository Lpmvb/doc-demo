module.exports = {
  css: {
    loaderOptions: {
      // 给 sass-loader 传递选项
      sass: {
        // 注意：在 sass-loader v8 中，这个选项名是 "prependData"
        additionalData: `@import "@ncfe/style.pc/dist/scss/base.extend.scss";\n@import "@ncfe/style.pc/dist/scss/base.scss";`,
      },
    },
  },
  chainWebpack: (config) => {
    config.module
      .rule("markdown")
      .test(/\.md$/)
      .use("due-loader")
      .loader("due-loader");
  },
};
